﻿namespace name.hangaoyang.VersionTransform
{
	partial class FormMain
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.numericUpDownVersion1 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDownVersion2 = new System.Windows.Forms.NumericUpDown();
			this.buttonTransform = new System.Windows.Forms.Button();
			this.textBoxDateTime = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownVersion1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownVersion2)).BeginInit();
			this.SuspendLayout();
			// 
			// numericUpDownVersion1
			// 
			this.numericUpDownVersion1.Location = new System.Drawing.Point(12, 12);
			this.numericUpDownVersion1.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
			this.numericUpDownVersion1.Name = "numericUpDownVersion1";
			this.numericUpDownVersion1.Size = new System.Drawing.Size(120, 21);
			this.numericUpDownVersion1.TabIndex = 0;
			this.numericUpDownVersion1.Click += new System.EventHandler(this.numericUpDownVersion1_Click);
			this.numericUpDownVersion1.Enter += new System.EventHandler(this.numericUpDownVersion1_Enter);
			// 
			// numericUpDownVersion2
			// 
			this.numericUpDownVersion2.Location = new System.Drawing.Point(12, 39);
			this.numericUpDownVersion2.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
			this.numericUpDownVersion2.Name = "numericUpDownVersion2";
			this.numericUpDownVersion2.Size = new System.Drawing.Size(120, 21);
			this.numericUpDownVersion2.TabIndex = 1;
			this.numericUpDownVersion2.Click += new System.EventHandler(this.numericUpDownVersion2_Click);
			this.numericUpDownVersion2.Enter += new System.EventHandler(this.numericUpDownVersion2_Enter);
			// 
			// buttonTransform
			// 
			this.buttonTransform.Location = new System.Drawing.Point(13, 67);
			this.buttonTransform.Name = "buttonTransform";
			this.buttonTransform.Size = new System.Drawing.Size(75, 23);
			this.buttonTransform.TabIndex = 2;
			this.buttonTransform.Text = "转换";
			this.buttonTransform.UseVisualStyleBackColor = true;
			this.buttonTransform.Click += new System.EventHandler(this.buttonTransform_Click);
			// 
			// textBoxDateTime
			// 
			this.textBoxDateTime.Location = new System.Drawing.Point(13, 97);
			this.textBoxDateTime.Name = "textBoxDateTime";
			this.textBoxDateTime.ReadOnly = true;
			this.textBoxDateTime.Size = new System.Drawing.Size(179, 21);
			this.textBoxDateTime.TabIndex = 3;
			// 
			// FormMain
			// 
			this.AcceptButton = this.buttonTransform;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(210, 139);
			this.Controls.Add(this.textBoxDateTime);
			this.Controls.Add(this.buttonTransform);
			this.Controls.Add(this.numericUpDownVersion2);
			this.Controls.Add(this.numericUpDownVersion1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "FormMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "版本号转换器";
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownVersion1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownVersion2)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBoxVersion1;
		private System.Windows.Forms.NumericUpDown numericUpDownVersion1;
		private System.Windows.Forms.NumericUpDown numericUpDownVersion2;
		private System.Windows.Forms.Button buttonTransform;
		private System.Windows.Forms.TextBox textBoxDateTime;
	}
}

