﻿using System;
using System.Windows.Forms;

namespace name.hangaoyang.VersionTransform
{
	public partial class FormMain : Form
	{
		public FormMain()
		{
			InitializeComponent();
		}

		private void buttonTransform_Click(object sender, EventArgs e)
		{
			DateTime MyDate = DateTime.Parse("2000-1-1").AddDays(Convert.ToInt32(numericUpDownVersion1.Value)).AddSeconds(Convert.ToInt32(numericUpDownVersion2.Value) * 2);
			textBoxDateTime.Text = MyDate.ToString("yyyy年MM月dd日 HH:mm:ss");
		}

		private void numericUpDownVersion1_Click(object sender, EventArgs e)
		{
			numericUpDownVersion1.Select(0, numericUpDownVersion1.Value.ToString().Length);
		}

		private void numericUpDownVersion1_Enter(object sender, EventArgs e)
		{
			numericUpDownVersion1.Select(0, numericUpDownVersion1.Value.ToString().Length);
		}

		private void numericUpDownVersion2_Click(object sender, EventArgs e)
		{
			numericUpDownVersion2.Select(0, numericUpDownVersion2.Value.ToString().Length);
		}

		private void numericUpDownVersion2_Enter(object sender, EventArgs e)
		{
			numericUpDownVersion2.Select(0, numericUpDownVersion2.Value.ToString().Length);
		}
	}
}